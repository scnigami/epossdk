# EPosSDK

[![CI Status](https://img.shields.io/travis/scnigami/EPosSDK.svg?style=flat)](https://travis-ci.org/scnigami/EPosSDK)
[![Version](https://img.shields.io/cocoapods/v/EPosSDK.svg?style=flat)](https://cocoapods.org/pods/EPosSDK)
[![License](https://img.shields.io/cocoapods/l/EPosSDK.svg?style=flat)](https://cocoapods.org/pods/EPosSDK)
[![Platform](https://img.shields.io/cocoapods/p/EPosSDK.svg?style=flat)](https://cocoapods.org/pods/EPosSDK)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

EPosSDK is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'EPosSDK'
```

## Author

scnigami, scnigami@gmail.com

## License

EPosSDK is available under the MIT license. See the LICENSE file for more info.
