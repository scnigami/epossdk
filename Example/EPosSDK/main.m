//
//  main.m
//  EPosSDK
//
//  Created by scnigami on 05/15/2019.
//  Copyright (c) 2019 scnigami. All rights reserved.
//

@import UIKit;
#import "EPOsAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([EPOsAppDelegate class]));
    }
}
